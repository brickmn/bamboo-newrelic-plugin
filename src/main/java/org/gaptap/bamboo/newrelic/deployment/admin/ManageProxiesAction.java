/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment.admin;

import org.apache.commons.lang.StringUtils;

/**
 * @author David Ehringer
 */
@SuppressWarnings("serial")
public class ManageProxiesAction extends BaseAdminAction {

    private int proxyId;
    private String name;
    private String host;
    private int port;
    private String description;

    private NewRelicAdminService adminService;

    public ManageProxiesAction(NewRelicAdminService adminService) {
        this.adminService = adminService;
    }

    public String doCreate() throws Exception {
        if (!isValidProxy()) {
            setMode(ADD);
            addActionError(getText("newrelic.admin.validation.errors"));
            return ERROR;
        }
        adminService.addSharedProxyServer(name, host, port, description);
        return SUCCESS;
    }

    public String doEdit() {
        setMode(EDIT);
        SharedProxyServer sharedProxy = adminService.getSharedProxyServer(proxyId);
        name = sharedProxy.getName();
       host = sharedProxy.getHost();
       port  = sharedProxy.getPort();
        description = sharedProxy.getDescription();
        return EDIT;
    }

    public String doUpdate() throws Exception {
        if (!isValidProxy()) {
            setMode(EDIT);
            addActionError(getText("newrelic.admin.validation.errors"));
            return ERROR;
        }
        adminService.updateSharedProxyServer(proxyId, name, host, port, description);
        return SUCCESS;
    }

    public String doConfirmDelete() {
        return SUCCESS;
    }

    public String doDelete() {
        // TODO check exists first
        adminService.deleteSharedProxyServer(proxyId);
        return SUCCESS;
    }

    private boolean isValidProxy() {
        if (StringUtils.isBlank(name)) {
            addFieldError("name", getText("newrelic.config.required.field"));
        }
        if (StringUtils.isBlank(host)) {
            addFieldError("host", getText("newrelic.config.required.field"));
        }
        // TODO port validation
        return !(hasFieldErrors() || hasActionErrors());
    }

    public int getProxyId() {
        return proxyId;
    }

    public void setProxyId(int proxyId) {
        this.proxyId = proxyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

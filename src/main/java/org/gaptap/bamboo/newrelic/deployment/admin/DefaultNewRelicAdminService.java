/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment.admin;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import org.apache.log4j.Logger;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;

/**
 * @author David Ehringer
 */
public class DefaultNewRelicAdminService implements NewRelicAdminService {

    private static final Logger LOG = Logger.getLogger(DefaultNewRelicAdminService.class);

    private final ActiveObjects ao;

    public DefaultNewRelicAdminService(ActiveObjects ao) {
        this.ao = ao;
    }

    @Override
    public SharedApiKey addSharedApiKey(final String name, final String apiKey, final String description) {
        LOG.debug("Adding new SharedApiKey with name " + name);
        return ao.executeInTransaction(new TransactionCallback<SharedApiKey>() {
            @Override
            public SharedApiKey doInTransaction() {
                SharedApiKey sharedApiKey = ao.create(SharedApiKey.class);
                overlayUpdates(sharedApiKey, name, apiKey, description);
                sharedApiKey.save();
                return sharedApiKey;
            }
        });
    }

    @Override
    public SharedApiKey updateSharedApiKey(final int id, final String name, final String apiKey,
            final String description) {
        return ao.executeInTransaction(new TransactionCallback<SharedApiKey>() {
            @Override
            public SharedApiKey doInTransaction() {
                SharedApiKey sharedApiKey = getSharedApiKey(id);
                // TODO not found condition
                overlayUpdates(sharedApiKey, name, apiKey, description);
                sharedApiKey.save();
                return sharedApiKey;
            }
        });
    }

    private void overlayUpdates(SharedApiKey sharedApiKey, String name, String apiKey, String description) {
        sharedApiKey.setApiKey(apiKey);
        sharedApiKey.setDescription(description);
        sharedApiKey.setName(name);
    }

    @Override
    public SharedApiKey getSharedApiKey(int id) {
        return ao.get(SharedApiKey.class, id);
    }

    @Override
    public List<SharedApiKey> allSharedApiKeys() {
        return newArrayList(ao.find(SharedApiKey.class));
    }

    @Override
    public void deleteSharedApiKey(final int id) {
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                SharedApiKey sharedApiKey = getSharedApiKey(id);
                ao.delete(sharedApiKey);
                return null;
            }
        });
    }

    @Override
    public SharedProxyServer addSharedProxyServer(final String name, final String host, final int port,final  String description) {
        LOG.debug("Adding new SharedApiKey with name " + name);
        return ao.executeInTransaction(new TransactionCallback<SharedProxyServer>() {
            @Override
            public SharedProxyServer doInTransaction() {
                SharedProxyServer sharedProxy = ao.create(SharedProxyServer.class);
                overlayUpdates(sharedProxy, name, host, port, description);
                sharedProxy.save();
                return sharedProxy;
            }
        });
    }

    @Override
    public SharedProxyServer updateSharedProxyServer(final int id, final String name,final  String host,final  int port, final String description) {
        return ao.executeInTransaction(new TransactionCallback<SharedProxyServer>() {
            @Override
            public SharedProxyServer doInTransaction() {
                SharedProxyServer sharedProxy = getSharedProxyServer(id);
                // TODO not found condition
                overlayUpdates(sharedProxy, name, host, port, description);
                sharedProxy.save();
                return sharedProxy;
            }
        });
    }

    private void overlayUpdates(SharedProxyServer sharedProxyServer, String name, String host, int port,  String description) {
        sharedProxyServer.setName(name);
        sharedProxyServer.setHost(host);
        sharedProxyServer.setPort(port);
        sharedProxyServer.setDescription(description);
    }

    @Override
    public SharedProxyServer getSharedProxyServer(int id) {
        return ao.get(SharedProxyServer.class, id);
    }

    @Override
    public List<SharedProxyServer> allSharedProxyServers() {
        return newArrayList(ao.find(SharedProxyServer.class));
    }

    @Override
    public void deleteSharedProxyServer(final int id) {
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                SharedProxyServer sharedProxy = getSharedProxyServer(id);
                ao.delete(sharedProxy);
                return null;
            }
        });
    }
}

/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.application;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.gaptap.bamboo.newrelic.NewRelicApiException;

/**
 * @author David Ehringer
 */
public class ApplicationInfo {

    private static final String MIME_TYPE = "application/json";

    private final String apiKey;
    private final String applicationId;
    private final HttpEntity httpEntity;

    public ApplicationInfo(String apiKey, String applicationId, File dataFile) {
        this.apiKey = apiKey;
        this.applicationId = applicationId;
        this.httpEntity = new FileEntity(dataFile, MIME_TYPE);
    }

    public ApplicationInfo(String apiKey, String applicationId, String requestContents) {
        this.apiKey = apiKey;
        this.applicationId = applicationId;
        try {
            this.httpEntity = new StringEntity(requestContents, MIME_TYPE, Charset.defaultCharset().toString());
        } catch (UnsupportedEncodingException e) {
            throw new NewRelicApiException("Unsupported encoding for: " + requestContents, e);
        }
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public HttpEntity getHttpEntity() {
        return httpEntity;
    }

    @Override
    public String toString() {
        return "ApplicationInfo [apiKey=" + apiKey + ", applicationId=" + applicationId + ", httpEntity=" + httpEntity
                + "]";
    }

}
